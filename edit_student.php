<?php 
include "./connection.php";
include "./parameter.php";
$id = isset($_REQUEST['id'])?$_REQUEST['id']:'';

if(empty($id)){
    die('Invalid id');
}
$student = null;

$sql = "SELECT * FROM students WHERE id=? LIMIT 1";
$stmt = $connect->prepare($sql);
$stmt->bind_param("i",$id);
$stmt->execute();

$results = $stmt->get_result();

while($row = $results->fetch_assoc()){
    $student=$row;
}

// error on array;
$errors = [];
//if submit is not fault
if (!empty($submit)) {
    // cek apakah empty atau tidak?
    if (empty($jenjang)||empty($gender)) {
        $errors[] = "Jenjang tidak boleh kosong!";
        $errors[] = "Gender belakang tidak boleh kosong!";
        $errors[] = "Hobi tidak boleh kosong!";
    } else {

        $hobbies = [];
        if (!empty($membaca)) {
            $hobbies[] = "membaca";
        }
        if (!empty($menulis)) {
            $hobbies[] = "menulis";
        }
        if (!empty($menggambar)) {
            $hobbies[] = "menggambar";
        }
        $hobi = implode(", ", $hobbies);
        $sql = "UPDATE students SET nama_depan='$nama_depan',nama_belakang='$nama_belakang',no_hp='$no_hp',gender='$gender',jenjang='$jenjang',hobi='$hobi',alamat='$alamat' WHERE id='$id'";

        $sukses = false;
        if ($connect->query($sql) == true) {
            echo "New Record edited successfully";
            session_start();
            $_SESSION['message'] = "Data Berhasil di Ubah";
            header('Location: list_student.php');
        } else {
            echo "Eror: " . $sql . "<br>" . $connect->error;
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Student Detail</title>
    <link rel="stylesheet" 
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" 
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" 
        crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" integrity="sha256-+N4/V/SbAFiW1MPBCXnfnP9QSN3+Keu+NlB+0ev/YKQ=" crossorigin="anonymous" />
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3>Edit Student</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row <?= empty($errors) ? "d-none" : "" ?>">
                    <div class="col-md-12">
                        <div class="alert alert-danger">
                            <ul><?php foreach ($errors as $error) : ?>
                                    <li><?= $error; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <form id="myForm" method="POST" action="edit_student.php?id=<?=$student['id'];?>">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Nama</span>
                        </div>
                        <input name="nama_depan" value="<?= $student['nama_depan'];?>" type="text" class="form-control" placeholder="Nama Depan" />
                        <input name="nama_belakang" value="<?= $student['nama_belakang'];?>" type="text" class="form-control" placeholder="Nama Belakang" />
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">No HP</span>
                        </div>
                        <input name="no_hp" value="<?= $student['no_hp'];?>" type="text" class="form-control" placeholder="No HP" />
                    </div>
                    <div class="form-group">
                    <small>Harap menyetel ulang gender jenjang dan hobi</small>
                        <h5>Gender</h5>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="gender" value="Pria">Pria
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="gender" value="Wanita">Wanita
                            </label>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Jenjang</span>
                        </div>
                        <select class="custom-select" name="jenjang" id="jenjang">
                            <option value="TK">TK</option>
                            <option value="SD">SD</option>
                            <option value="SMP">SMP</option>
                            <option value="SMA">SMA</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <h5>Hobi</h5>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input id="membaca" type="checkbox" class="form-check-input" name="membaca" value="Mambaca">Membaca
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input id="menulis" type="checkbox" class="form-check-input" name="menulis" value="Menulis">Menulis
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input id="menggambar" type="checkbox" class="form-check-input" name="menggambar" value="Menggambar">Menggambar
                            </label>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Alamat</span>
                        </div>
                        <textarea name="alamat" type="text" class="form-control" placeholder="Masukan Alamat" rows="4"><?= $student['alamat'];?></textarea>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="submit" value="submit" name="submit" id="submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" 
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" 
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
    integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
    crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" 
    integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" 
    crossorigin="anonymous"></script>