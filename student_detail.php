<?php 
include "./connection.php";
$id = isset($_REQUEST['id'])?$_REQUEST['id']:'';

if(empty($id)){
    die('Invalid id');
}
$student = null;

$sql = "SELECT * FROM students WHERE id=? LIMIT 1";
$stmt = $connect->prepare($sql);
$stmt->bind_param("i",$id);
$stmt->execute();

$results = $stmt->get_result();

while($row = $results->fetch_assoc()){
    $student=$row;

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Student Detail</title>
    <link rel="stylesheet" 
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" 
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" 
        crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" integrity="sha256-+N4/V/SbAFiW1MPBCXnfnP9QSN3+Keu+NlB+0ev/YKQ=" crossorigin="anonymous" />
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3>Student Detail</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card bg-info text-white">
                    <div class="card-header"><?= $student['nama_depan'] . ' ' . $student['nama_belakang']; ?></div>
                    <div class="card-body">
                        <ul style="list-style: none;">
                            <li><i class="fas fa-paw"></i>&nbsp;<strong>Jenjang : </strong> <?= $student['jenjang']?></li>
                            <li><i class="fas fa-mobile-alt"></i>&nbsp;<strong>No HP : </strong> <?= $student['no_hp']?></li>
                            <li><i class="fas fa-crown"></i><strong>&nbsp;Hobi : </strong> <?= $student['hobi']?></li>
                            <li><i class="fas fa-map-marked-alt"></i><strong>&nbsp;Alamat : </strong> <?= $student['alamat']?></li>
                        </ul>
                    </div>
                    <div class="card-footer">
                        <a href="./list_student.php" class="btn text-white"><i class="fas fa-arrow-left"></i> Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" 
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" 
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
    integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
    crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" 
    integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" 
    crossorigin="anonymous"></script>