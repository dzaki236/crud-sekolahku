<?php
include "./connection.php";
include "./parameter.php";
$students = [];
$total = 0;
$sql = "SELECT * FROM students";
$results = $connect->query($sql);
$total = $results->num_rows;
while ($row = $results->fetch_assoc()) {
    $students[] = $row;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Student</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
</head>

<body>
    <div class="container-fluid">
        <h3>Data Student</h3>
        <?php session_start();
        if(isset($_SESSION['message'])){
            $message = $_SESSION['message'];
            $_SESSION=[];
            session_unset();
            session_destroy();
        }
        ?>
        <div class="row <?= empty($message) ? "d-none" : "" ?>">
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <i class="fa fa-check"><?= $message;?></i>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <a href="./add_student.php" class="btn btn-outline-dark mb-4 mt-3">[+] Add Students</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>No Hp</th>
                            <th>Gender</th>
                            <th>Jenjang</th><th>Alamat</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php foreach ($students as $k => $student) : ?><tr>
                                <td><?= $k++; ?></td>
                                <td><?= $student['nama_depan'] . ' ' . $student['nama_belakang']; ?></td>
                                <td><?= $student['no_hp'];?></td>
                                <td><?= $student['gender']; ?></td>
                                <td><?= $student['jenjang']; ?></td>
                                <td><?= $student['alamat']; ?></td>
                                <td>
                                <form action="delete_student.php" method="POST">
                                <div class="btn-group">
                                <a href="./student_detail.php?id=<?= $student['id'];?>" class="btn btn-primary ml-3 btn-sm"><i class="fas fa-eye"></i></a>
                                <a href="./edit_student.php?id=<?= $student['id'];?>" class="btn btn-info ml-3 btn-sm"><i class="fas fa-pencil-alt"></i></a>
                                <input type="hidden" name="id" value="<?=$student['id'];?>">
                                <button type="submit" onclick="return confirm('are u sure to delete?');" class="btn btn-danger ml-3 btn-sm"><i class="fas fa-trash"></i></a>

                                </div></form></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
</body>

</html>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/fontawesome.min.js" integrity="sha512-KCwrxBJebca0PPOaHELfqGtqkUlFUCuqCnmtydvBSTnJrBirJ55hRG5xcP4R9Rdx9Fz9IF3Yw6Rx40uhuAHR8Q==" crossorigin="anonymous"></script>