<?php
include "./connection.php";
//delete data
$id = isset($_REQUEST['id'])?$_REQUEST['id']:'';

if(empty($id)){
    die('invalid student id');
}
$student = null;
$sql = "DELETE FROM students WHERE id=? LIMIT 1";
$stmt = $connect->prepare($sql);
$stmt->bind_param("i",$id);
$stmt ->execute();

if($stmt ->affected_rows > 0){
    session_start();
    $_SESSION['message']="data berhasil di hapus";
    header('Location: list_student.php');
}
?>