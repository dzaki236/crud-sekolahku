<?php
include "./connection.php";
include "./parameter.php";
// error on array;
$errors = [];
//if submit is not fault
if (!empty($submit)) {
    // cek apakah empty atau tidak?
    if (empty($nama_depan)||empty($nama_belakang)||empty($no_hp)) {
        $errors[] = "Nama depan tidak boleh kosong!";
        $errors[] = "Nama belakang tidak boleh kosong!";
        $errors[] = "No Hp tidak boleh kosong!";
    } else {

        $hobbies = [];
        if (!empty($membaca)) {
            $hobbies[] = "membaca";
        }
        if (!empty($menulis)) {
            $hobbies[] = "menulis";
        }
        if (!empty($menggambar)) {
            $hobbies[] = "menggambar";
        }
        $hobi = implode(", ", $hobbies);
        $sql = "INSERT INTO students VALUES (NULL,'{$nama_depan}','{$nama_belakang}','{$no_hp}','{$gender}','{$jenjang}','{$hobi}','{$alamat}')";
        if ($connect->query($sql) == true) {
            echo "New Record created successfully";
            session_start();
            $_SESSION['message'] = "Data Berhasil di tambah!";
            header('Location: list_student.php');
        } else {
            echo "Eror: " . $sql . "<br>" . $connect->error;
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Student</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <div class="container-fluid">
        <h3>Add Student</h3>

        <div class="row">
            <div class="col-md-12">
                <div class="row <?= empty($errors) ? "d-none" : "" ?>">
                    <div class="col-md-12">
                        <div class="alert alert-danger">
                            <ul><?php foreach ($errors as $error) : ?>
                                    <li><?= $error; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <form id="myForm" method="POST" action="add_student.php">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Nama</span>
                        </div>
                        <input name="nama_depan" value="" type="text" class="form-control" placeholder="Nama Depan" />
                        <input name="nama_belakang" value="" type="text" class="form-control" placeholder="Nama Belakang" />
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">No HP</span>
                        </div>
                        <input name="no_hp" value="" type="text" class="form-control" placeholder="No HP" />
                    </div>
                    <div class="form-group">
                        <h5>Gender</h5>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="gender" value="Pria">Pria
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="gender" value="Wanita">Wanita
                            </label>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Jenjang</span>
                        </div>
                        <select class="custom-select" name="jenjang" id="jenjang">
                            <option value="TK">TK</option>
                            <option value="SD">SD</option>
                            <option value="SMP">SMP</option>
                            <option value="SMA">SMA</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <h5>Hobi</h5>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input id="membaca" type="checkbox" class="form-check-input" name="membaca" value="Mambaca">Membaca
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input id="menulis" type="checkbox" class="form-check-input" name="menulis" value="Menulis">Menulis
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input id="menggambar" type="checkbox" class="form-check-input" name="menggambar" value="Menggambar">Menggambar
                            </label>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Alamat</span>
                        </div>
                        <textarea name="alamat" type="text" class="form-control" placeholder="Masukan Alamat" rows="4"></textarea>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="submit" value="submit" name="submit" id="submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

</html>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>